v {xschem version=3.1.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 400 -300 440 -300 {
lab=VSS}
N 220 -270 220 -220 {
lab=VSS}
N 220 -220 440 -220 {
lab=VSS}
N 440 -270 440 -220 {
lab=VSS}
N 220 -300 280 -300 {
lab=VSS}
N 280 -300 280 -260 {
lab=VSS}
N 220 -260 280 -260 {
lab=VSS}
N 370 -300 400 -300 {
lab=VSS}
N 370 -300 370 -260 {
lab=VSS}
N 370 -260 440 -260 {
lab=VSS}
N 220 -380 220 -330 {
lab=O}
N 220 -380 440 -380 {
lab=O}
N 440 -380 440 -330 {
lab=O}
N 330 -460 330 -380 {
lab=O}
N 330 -560 330 -520 {
lab=p_mid}
N 330 -690 330 -620 {
lab=VDD}
N 60 -590 290 -590 {
lab=I0}
N 130 -300 180 -300 {
lab=I0}
N 370 -490 590 -490 {
lab=I1}
N 480 -300 530 -300 {
lab=I1}
N 330 -420 370 -420 {
lab=O}
N 230 -490 330 -490 {
lab=VDD}
N 230 -660 230 -490 {
lab=VDD}
N 230 -660 330 -660 {
lab=VDD}
N 330 -590 400 -590 {
lab=VDD}
N 400 -630 400 -590 {
lab=VDD}
N 330 -630 400 -630 {
lab=VDD}
N 130 -590 130 -300 {
lab=I0}
N 530 -490 530 -300 {
lab=I1}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 200 -300 0 0 {name=XN0
w=4
l=18n
nf=2
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 350 -490 0 1 {name=XP1
w=10
l=18n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/ipin.sym} 60 -590 2 1 {name=p3 lab=I0}
C {devices/opin.sym} 370 -420 0 0 {name=p4 lab=O}
C {devices/iopin.sym} 210 -100 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 210 -80 2 0 {name=p6 lab=VSS}
C {devices/lab_pin.sym} 330 -690 2 0 {name=l2 sig_type=std_logic lab=VDD}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 470 -90 0 0 {name=XDUM
w=4
l=18n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 460 -300 0 1 {name=XN1
w=4
l=18n
nf=2
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 330 -220 1 1 {name=l7 sig_type=std_logic lab=VSS}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 310 -590 0 0 {name=XP0
w=10
l=18n
nf=2
model=pmos4_lvt
spiceprefix=X
}
C {devices/ipin.sym} 590 -490 2 0 {name=p7 lab=I1}
C {devices/lab_pin.sym} 330 -540 2 1 {name=l1 sig_type=std_logic lab=midp}
