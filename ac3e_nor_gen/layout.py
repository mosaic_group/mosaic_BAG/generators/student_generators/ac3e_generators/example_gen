from typing import *
from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID

from sal.row import *
from sal.transistor import *

from .params import ac3e_nor_layout_params


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='ac3e_nor_layout_params parameter object',
        )

    def draw_layout(self):
        params: ac3e_nor_layout_params = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.MX,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['I0', 'I1'],
                           wire_names_ds=['VSS', 'O']
                           )

        row_nmos = Row(name='n',
                       orientation=RowOrientation.MX,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['I0', 'I1'],
                       wire_names_ds=['O','VSS']
                       )

        row_Dummy_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.MX,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['I0', 'I1'],
                           wire_names_ds=['VSS', 'O']
                           )

        row_Dummy_PB = Row(name='Dummy_PB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PB'],
                           threshold=params.th_dict['Dummy_PB'],
                           wire_names_g=['I0', 'I1'],
                           wire_names_ds=['VDD', 'O']
                           )

        row_pmos = Row(name='p',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['I0', 'I1'],
                       wire_names_ds=[ 'VDD','midp', 'O']
                       )
        
        ## Placing XP1 in a seperate row so as to enable series connection with XP0,
        ## Irrespective of number of PMOS fingers
        row_pmos_2 = Row(name='p',                      
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['I0', 'I1'],
                       wire_names_ds=[ 'VDD','midp', 'O']
                       )

        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['I0', 'I1'],
                           wire_names_ds=['VDD', 'O']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_NB, row_nmos, row_Dummy_NT, row_Dummy_PB, row_pmos_2, row_pmos, row_Dummy_PT])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n'] // divide
        fg_p = params.seg_dict['p'] // divide

        nmos0 = Transistor(name='xn0', row=row_nmos, fg=fg_n, seff_net='VSS', deff_net='O')
        nmos1 = Transistor(name='xn1', row=row_nmos, fg=fg_n, seff_net='VSS', deff_net='O')
        pmos0 = Transistor(name='xp0', row=row_pmos, fg=fg_p, seff_net='VDD', deff_net='midp')
        pmos1 = Transistor(name='xp1', row=row_pmos_2, fg=fg_p, seff_net='midp', deff_net='O')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [nmos0, nmos1, pmos0, pmos1]

        # 3:   Calculate transistor locations
        fg_single = max(nmos0.fg, nmos1.fg, pmos0.fg, pmos1.fg)
        fg_total = 2*fg_single + 2 * params.ndum
        fg_dum = params.ndum

        if (pmos0.fg - nmos0.fg) % 4 != 0:
            raise ValueError('We assume seg_p and seg_n differ by multiples of 4.')

        # Calculate positions of transistors
        nmos0.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)
        nmos1.assign_column(offset=fg_dum, fg_col=3*fg_single, align=TransistorAlignment.CENTER)
        pmos0.assign_column(offset=fg_dum//2, fg_col=fg_single, align=TransistorAlignment.CENTER)
        pmos1.assign_column(offset=fg_dum, fg_col=3*fg_single, align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        nmos0.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        nmos1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos0.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        pmos1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Define horizontal tracks on which connections will be made
        row_nmos0_idx = rows.index_of_same_channel_type(row_nmos)
        row_nmos1_idx = rows.index_of_same_channel_type(row_nmos)
        row_pmos0_idx = rows.index_of_same_channel_type(row_pmos)
        row_pmos1_idx = rows.index_of_same_channel_type(row_pmos_2)

        tid_nmos0_G = self.get_wire_id('nch', row_nmos0_idx, 'g', wire_name='I0')
        tid_nmos1_G = self.get_wire_id('nch', row_nmos1_idx, 'g', wire_name='I1')
        tid_pmos0_G = self.get_wire_id('pch', row_pmos0_idx, 'g', wire_name='I0')
        tid_pmos1_G = self.get_wire_id('pch', row_pmos1_idx, 'g', wire_name='I1')

        tid_nmos0_D = self.get_wire_id('nch', row_nmos0_idx, 'ds', wire_name='O')
        tid_nmos1_D = self.get_wire_id('nch', row_nmos1_idx, 'ds', wire_name='O')
        tid_pmos0_D = self.get_wire_id('pch', row_pmos0_idx, 'ds', wire_name='midp')
        tid_pmos1_D = self.get_wire_id('pch', row_pmos1_idx, 'ds', wire_name='O')

        # tid_nmos0_S = self.get_wire_id('nch', row_nmos0_idx, 'ds', wire_name='VSS')
        # tid_nmos1_S = self.get_wire_id('nch', row_nmos1_idx, 'ds', wire_name='VSS')
        tid_pmos1_S = self.get_wire_id('pch', row_pmos1_idx, 'ds', wire_name='midp')
        tid_pmos0_S = self.get_wire_id('pch', row_pmos0_idx, 'ds', wire_name='VDD')

        # 7:  Perform wiring
        # #gates
        warr_n0_G = self.connect_to_tracks([nmos0.g], tid_nmos0_G)
        warr_n1_G = self.connect_to_tracks([nmos1.g], tid_nmos1_G)                                                                                                                                                                                                          
        warr_p0_G = self.connect_to_tracks([pmos0.g], tid_pmos0_G)
        warr_p1_G = self.connect_to_tracks([pmos1.g], tid_pmos1_G)

        warr_I0 = self.connect_to_tracks([pmos0.g, nmos0.g], tid_nmos0_G)
        warr_I1 = self.connect_to_tracks([pmos1.g, nmos1.g], tid_nmos1_G)

        # #drain
        warr_n0_D = self.connect_to_tracks([nmos0.d], tid_nmos0_D)
        warr_n1_D = self.connect_to_tracks([nmos1.d], tid_nmos1_D)  
        warr_nx_D = self.connect_to_tracks([nmos0.d, nmos1.d], tid_nmos0_D)  

        warr_p0_D = self.connect_to_tracks([pmos0.d], tid_pmos0_D)
        warr_p1_D = self.connect_to_tracks([pmos1.d], tid_pmos1_D)
        
        #source
        # warr_n0_S = self.connect_to_tracks([nmos0.s], tid_nmos0_S)
        # warr_n1_S = self.connect_to_tracks([nmos1.s], tid_nmos1_S)

        #warr_p0_S = self.connect_to_tracks([pmos0.s], tid_pmos0_S)
        warr_p1_S = self.connect_to_tracks([pmos1.s], tid_pmos1_S)
        warr_midp = self.connect_to_tracks([pmos0.d, pmos1.s], tid_pmos0_D)
                                                                                                                                                                                                                    

        # # Define vertical tracks for the connections, based on the location of the col 
        tid_S_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord=self.layout_info.col_to_coord(
                    col_idx=fg_dum,
                    unit_mode=True
                ),
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig1')
        )

        # Connection of transistors sources
        self.connect_to_tracks(
            [warr_nx_D, warr_p1_D],
            tid_S_vert,
            min_len_mode=0,
        )

        # self.connect_to_tracks(
        #     [pmos0.g, nmos0.g],
        #     tid_S_vert,
        #     min_len_mode=0,
        # )

        # # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()

        self.connect_to_substrate('ptap',[nmos0.s])
        self.connect_to_substrate('ptap',[nmos1.s])
        self.connect_to_substrate('ntap',[pmos0.s])

        # Add Pin
        self.add_pin('I0', [warr_I0], show=params.show_pins)
        self.add_pin('I1', [warr_I1], show=params.show_pins)
        self.add_pin('O', [warr_nx_D], show=params.show_pins)
        # self.add_pin('VSS', [warr_n0_S, warr_n1_S], show=params.show_pins)
        # self.add_pin('VDD', warr_p0_S, show=params.show_pins)

        # # export supplies
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class ac3e_nor(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
